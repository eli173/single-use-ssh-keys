
SINGLE-USE SSH KEYS

Ever wanted a way to get into a server exactly once? Want a backup key with little margin for error? Look no further!

This simple script will take any public-key on your server and enable it as an SSH key that can be used only once. This works by putting an entry in your `~/.ssh/authorized_keys` which has a command that deletes the entry from itself before dropping the user into their login shell. It's a simple and effective system.

The only real requirement to use this as intended is that your login shell should be able to take `--login` as an argument.

To add a key, just run the script with your public key file as the only argument, like `./single_use_add_key.sh <filename>.pub`. Your file must be a typical `ssh-keygen` key. This adds the key (and its removal functionality) to your `authorized-keys`, and you use your private key as you would use any other ssh private key.

Other than the mostly inane uses detailed above, this serves as a nice system to allow an action do be done precisely once per key. You can edit the script and replace `\$SHELL --login` with a command of your choosing. Perhaps you want to print a message once, perhaps you want to allow someone to run a command a set number of times (so you just generate that set number of keys). There are a wide variety of things one could do with this, from practical applications to practical jokes to works of art to single-use passes.


There is the potential for a timing issue, if two people attempt to use the same certificate at the same time, then it's somewhat possible that both might be able to get in. I've attempted to alleviate this a little in the code, but the possibility remains, and I don't want to take more extreme or restrictive measures at this point.



