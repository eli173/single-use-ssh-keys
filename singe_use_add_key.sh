#!/bin/sh

if [ "$#" -ne 1 ] || ! [ -f "$1" ]; then
    echo "Usage: $0 <file.pub>"
    exit 1
fi

KEYSTR=$(cat $1 | awk '{print $2}')

COMMAND="if [ \$(grep '$KEYSTR' ~/.ssh/authorized_keys | wc -l) -eq 0 ]; then echo 'no go'; exit 1; fi; sed -i '\|$KEYSTR|d' ~/.ssh/authorized_keys; \$SHELL --login"

ENTRY="command=\"$COMMAND\" $(cat $1)"

echo $ENTRY >> ~/.ssh/authorized_keys

echo "added key as single-use"
